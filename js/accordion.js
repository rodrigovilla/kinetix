// Documentation sidebar accordion

$(function(){
	var Accordion = function(el, multiple) {
		this.el = el || {};
		this.multiple = multiple || false;
		// Variables
		var link = this.el.find('.link');
		// Events
		link.on('click', {el: this.el, multiple: this.multiple},this.dropdown)
	}

	Accordion.prototype.dropdown = function(e) {
		var  $el = e.data.el;
			$this = $(this),
			$next = $this.next();

		// Unchains opening event on elements following the link [li] class
		$next.slideToggle();
		// Adds class 'open' to parent element of link [li] class
		$this.parent().toggleClass('open');

		//Parameter allowing only 1 sub-menu open
		if(!e.data.multiple){
			$el.find('.submenu').not($next).slideUp().parent().removeClass('open');
		}
	}
		// Multiple sub-menus [true] or one at once [false]
		var accordion = new Accordion($('#accordion'), false);
});
