<?php
if($_POST)
{
	$to_email = "info@rodrigo-villa.com"; // Please replace it with own email here.

	// Check if its an ajax request, exit if not.
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {

		$output = json_encode(array( // Create JSON data.
			'type'=>'error',
			'text' => 'Sorry Request must be Ajax POST'
		));
		die($output); // Exit script outputting json data.
    }

	// Sanitize input data using PHP filter_var().
	$user_name		= filter_var($_POST["user_name"], FILTER_SANITIZE_STRING);
    $lastname		= filter_var($_POST["lastname"], FILTER_SANITIZE_STRING);
	$user_email		= filter_var($_POST["user_email"], FILTER_SANITIZE_EMAIL);
	$budget     	= filter_var($_POST["budget"], FILTER_SANITIZE_STRING);
	$message		= filter_var($_POST["msg"], FILTER_SANITIZE_STRING);

	// Additional PHP validation.
	if(strlen($user_name)<3){ // If length is less than 4 it will output JSON error.
		$output = json_encode(array('type'=>'error', 'text' => 'Name is too short or empty!'));
		die($output);
	}
	if(!filter_var($user_email, FILTER_VALIDATE_EMAIL)){ // Email validation.
		$output = json_encode(array('type'=>'error', 'text' => 'Please enter a valid email!'));
		die($output);
	}
	if(strlen($message)<3){ // Check emtpy message.
		$output = json_encode(array('type'=>'error', 'text' => 'Too short message! Please enter something.'));
		die($output);
	}

	// Email body.
	$message_body = $message ."<br><br>\r\n\r\n<b>Name: </b>".$user_name."<span> </span>".$lastname."<br>\r\n<b>Email: </b>".$user_email."<br>\r\n<b>Budget: </b>".$budget. "<br>\r\n<b>Website: </b>" ;

	// Proceed with PHP email.
	$subject = 'UI Design: Rodrigo Villa';
    $headers.= "Content-Type: text/html\r\n";
	$headers.= 'From: '.$user_name.'<'.$user_email.'>'.'' . "\r\n" .
	'Reply-To: '.$user_email.'' . "\r\n" .
	'X-Mailer: PHP/' . phpversion();
   


	$send_mail = mail($to_email, $subject, $message_body, $headers);

	if(!$send_mail)
	{
		// If mail couldn't be sent output error. Check your PHP email configuration (if it ever happens)
		$output = json_encode(array('type'=>'error', 'text' => 'Could not send mail! Please check your PHP mail configuration.'));
		die($output);
	}else{
		$output = json_encode(array('type'=>'message', 'text' => '<h3>'.$user_name .'</h3>' . '<span>Thanks! I\'ll contact you as soon as possible.</span>'));
		die($output);
	}
}
?>
